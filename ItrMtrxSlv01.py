# load library
import numpy as np
import time as tm

# give the entries in matrix A and column vector f
A = np.array([[-3, 1, 2],[-1, -3, 1],[-1, 3, 4]])  

f = np.array([2, 6, 4])

# decompose A to D and R matrices
D = np.diag(np.diag(A))
R = A - D
## Test add a line to the file in branch.

# inverse of D matrix
D_inv = np.linalg.inv(D)
    

# initial guess value of x
x = np.array([0, 0, 0])

# walltime before matrix solving
cpu_tm_bg = tm.time()

# iteration
for iter in range(200):
    # use function np.matmul for matrix-vector multiplication
    temp = np.matmul(D_inv, (f-np.matmul(R,x)))
    x    = temp

    # residual during iterations
    r    = f - np.matmul(A,x)
    
    # use calculate the norm of the residual using numpy function
    n = 0
    for i in r:
      n += i**2
    nrm = np.sqrt(n)


    

# walltime after matrix solving
cpu_tm_ed = tm.time()    

# print results
print(cpu_tm_ed - cpu_tm_bg, nrm)

